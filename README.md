# gencodegouv

Generate static HTML pages from [Open Software Base](https://git.framasoft.org/etalab/open-software-base-yaml).

## Install

Download [this program](https://git.framasoft.org/etalab/gencodegouv):
```bash
git clone https://git.framasoft.org/etalab/gencodegouv.git
```

Download the [Open Software Base (in YAML format)](https://git.framasoft.org/etalab/open-software-base-yaml):
```bash
git clone https://git.framasoft.org/etalab/open-software-base-yaml.git
```

## Usage

```bash
cd gencodegouv/
./gencodegouv.py ../open-software-base-yaml html
```
